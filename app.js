const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
const logger = require("morgan");
const app = express();
var CronJob = require("node-cron");
console.log(typeof(CronJob))
const client = mongoose.connect("mongodb://mongo:27017/GameDB").then(() => console.log("connected to aggregation database!"));

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.disable('x-powered-by');


const gameSchema = new mongoose.Schema({
    title: String,
    kebabTitle: String,
    magnetlink: String,
    size: Number,
    files: Number,
    review: Number,
    genre: String,
    thumbnail: String,
    old_thumbnail: String,
    backgroundimg: String,
    old_backgroundimg: String,
    money_link: String,
    money_linklol: String,
    trailerlink: String,
    count: {
      type: Number,
      default: 0,
    },
    comments: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Comment",
      },
    ],
    updatesNdlcs: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Updates",
      },
    ],
  })
Game = mongoose.model("Game", gameSchema)
let final_data = []

const job = async () => {
    let arr = await Game.aggregate([{ "$sample": { "size": 8 } }]);
    console.log(typeof(arr));
    final_data = JSON.stringify(arr);
    console.log(JSON.stringify(arr));
    return final_data;
}
console.log(typeof(CronJob));
CronJob.schedule("0 0 0 * * *", job);

app.get('/aggregate', async (req, res) => {
    if (final_data.length === 0) {
	const data = await job();
	return res.json(data);
    }
    res.json(final_data);
})
app.listen(9091, () => console.log("running!"));
